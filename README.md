# ruted-vim

The ruted-vim plugin is part of √𝚎𝚍, the rich Unicode text editing (RUTED,
pronounced ˈruːtɪd) suite. Ruted-vim allows you to enter Unicode symbols
easily in [Vim](https://www.vim.org/). For example, to enter "⇒" while in Vim
insert mode, you switch to the *arrow* symbol mode and type `=>`. Ruted-vim
automatically replaces "=>" by "⇒".  Ruted-vim uses
[ruted-format](https://gitlab.com/ruted/ruted-format) as the source for the
available symbol modes and their replacement rules.

## Licence

Ruted-vim is free software; ruted-vim is licensed under the [EUPL
1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12). You find
the source code for ruted-vim in its [repository on
GitLab.com](https://gitlab.com/ruted/ruted-vim).

## Prerequisites

Ruted-vim has two prerequisites:

1. Ruted-vim uses the program
   [ruted-format](https://gitlab.com/ruted/ruted-format). Install it before
   you install ruted-vim.

2. To render the Unicode symbols you enter in Vim, you need a Unicode font
   that support these symbols. You find fairly complete Unicode fonts on most
   modern operating systems, but you can also install new ones. See
   [https://en.wikipedia.org/wiki/Open-source_Unicode_typefaces](https://en.wikipedia.org/wiki/Open-source_Unicode_typefaces)
   for a list of open-source Unicode fonts.

   After installing a suitable Unicode font on your operating system, use that
   font in the terminal you use Vim in. Or, if you use GVim, set the
   [`guifont`](https://vimhelp.org/gui.txt.html#gui-font) property.

## Install ruted-vim

Install ruted-vim like any other Vim plugin. If you use Vim's help system, you
can also [install the accompanying help
file](https://vimhelp.org/usr_05.txt.html#add-local-help).  This help file
contains the same information as README.md file in ruted-vim's GitLab
repository.

## Use ruted-vim

After installing the ruted-vim plugin, Vim automatically enables ruted-vim
when you start Vim. Ruted-vim only works in Vim's *insert* mode. The plugin
replaces source strings with a Unicode symbol according to the replacement
rules of the symbol mode you selected. At start-up, ruted-vim selects
the *NONE* symbol mode which does not have any replacement rules. In this *NONE*
symbol mode Vim works as if you did not install the ruted-vim plugin.

|key|mode             |
|---|-----------------|
|a  |arrows           |
|b  |bold             |
|C  |bold calligraphy |
|F  |bold Fraktur     |
|G  |bold greek       |
|I  |bold italic      |
|c  |calligraphy      |
|e  |encircled        |
|f  |fraktur          |
|g  |greek            |
|i  |italic           |
|m  |mathematics      |
|o  |open             |
|p  |programming      |
|_  |subscript        | 
|^  |superscript      |

While in Vim's *insert* mode, you can switch to another symbol mode using that
mode's key. In the table above, you see some symbol modes installed
with the program ruted-format. For example, to switch to the "mathematics"
symbol mode, use key combination `<C-m>m`. In this mathematics symbol mode,
you can enter `NN` to get "ℕ", `forall` to get "∀", `<=` to get "≤", and so
on.  To leave the mathematics symbol mode again, either select another symbol
mode, or use key combination `<C-m><Space>` to go back to the *NONE* symbol
mode.

## Configure ruted-vim

You can configure the following of ruted-vim's properties:

- `g:ruted_command`

  By default, ruted-vim expects the program ruted-format on the user's PATH.
  If it is not, or you want to use another program as source for symbol modes
  and their replacement rules, assign that program's path to
  `g:ruted_command`.

- `g:ruted_mode_selection_key`

  By default, ruted-vim uses `<C-m>` as the symbol mode selection key. Assign
  another key to `g:ruted_mode_selection_key` to change this default.

## Ruted-vim's commands

The ruted-vim plugin defines the following commands:

- `StartRuted()`

  Call `StartRuted()` to enable the ruted-vim plugin.

- `StopRuted()`

  Call `StopRuted()` to disable the ruted-vim plugin.

- `RutedStatus()`

  The `RutedStatus()` command returns a string showing ruted-vim's status. If
  ruted-vim is disabled, `RutedStatus()` returns the empty string. Otherwise,
  it returns either "√𝚎𝚍" if the *NONE* symbol mode is active, or "√𝚎𝚍=mode
  name" if another symbol mode is active.

  I use `RutedStatus()` in my
  [statusline](https://vimhelp.org/options.txt.html#%27statusline%27).
