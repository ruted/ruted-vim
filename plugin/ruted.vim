" Use ruted to easily enter Unicode symbols in Vim.
"
" Copyright 2020, 2021 Huub de Beer <huub@campinecomputing.eu>
"
" Licensed under the EUPL, Version 1.2 or later. You may not use this work
" except in compliance with the EUPL. You may obtain a copy of the EUPL at:
"
" https://joinup.ec.europa.eu/software/page/eupl
"
" Unless required by applicable law or agreed to in writing, software
" distributed under the EUPL is distributed on an "AS IS" basis, WITHOUT
" WARRANTY OR CONDITIONS OF ANY KIND, either express or implied. See the EUPL
" for the specific language governing permissions and limitations under the
" licence.

" Load ruted-vim only once.
if exists('g:loaded_ruted')
  finish
endif
let g:loaded_ruted = 1

""
" Constants
""

let s:ESCAPE_CHARS          = '|/'              " Escape these characters when they occur in a symbol's source string.
let s:RUTED_NAME            = '√𝚎𝚍'             " Ruted's name. Used for communication with the user.
let s:RUTED_NONE_MODE       = 'NONE'            " In mode NONE, ruted does not replace any symbols.
let s:RUTED_MENU            = "Plugin.Ruted"    " Ruted's Plugin submenu.

""
" Variables
""

let s:ruted_on              = 0                 " The ruted Vim plugin is on (=1) or off (=0).
let s:ruted_available_modes = {}                " The available modes defined in ruted-format. This variable is populated when ruted is started. See #StartRuted().
let s:active_mode           = {}                " The active mode. If {}, the current mode is NONE.
let s:active_key            = s:RUTED_NONE_MODE " The key of the active mode. Ruted starts in mode NONE.

" Set the path to the ruted-format program. In most cases, the ruted-format
" program is on the user's PATH. Change this default by assigning to
" 'g:ruted_format'.
let s:ruted_command = 'ruted-format'
if exists('g:ruted_command')
  let s:ruted_command = g:ruted_command
endif

" Ruted's default mode when ruted is enabled. By default mode NONE is used,
" but you can set any mode by assigning a mode's name to
" 'g:ruted_default_mode'.
let s:ruted_default_mode = s:RUTED_NONE_MODE
if exists('g:ruted_default_mode')
  let s:ruted_default_mode = g:ruted_default_mode
endif

" Mode switching key. By default CONTROL+m is used, but you can set any key
" combination you like by assigning that key combination to
" 'g:ruted_mode_selection_key'.
let s:ruted_mode_selection_key = '<C-m>'
if exists('g:ruted_mode_selection_key') 
  let s:ruted_mode_selection_key = g:ruted_mode_selection_key
endif

""
" Private functions
""

function s:RutedInitialize()
  let s:active_key = s:RUTED_NONE_MODE
  let s:active_mode = {}
endfunction

function s:RutedInstall(mode)
  for l:rule in a:mode['rules']
    execute 'inoremap ' . escape(l:rule['source'], s:ESCAPE_CHARS) . ' ' . l:rule['symbol']
  endfor
endfunction

function s:RutedUninstall(mode)
  for l:rule in a:mode['rules']
    execute 'iunmap ' . escape(l:rule['source'], s:ESCAPE_CHARS)
  endfor
endfunction

function s:RutedSwitchMode(key)
  " get new mode somehow, first check if available
  " if not, install default.
  if !empty(s:active_mode)
    call s:RutedUninstall(s:active_mode)
  endif

  if a:key == s:RUTED_NONE_MODE
    call s:RutedInitialize()
  elseif has_key(s:ruted_available_modes, a:key)
    silent let l:json = system(s:ruted_command . ' show ' . a:key . ' --json')
    let s:active_mode = json_decode(l:json)
    call s:RutedInstall(s:active_mode)
  else
    echo 'No such key ' a:key
  end
endfunction

function s:RutedMenuItem(key, name, shortcut)
  let l:menu = escape(s:RUTED_MENU . '.' . a:name . '<TAB>' . a:shortcut, " ") 
  execute 'inoremenu <silent> ' . l:menu . ' <esc>:call <SID>RutedSwitchMode("' . a:key . '")<CR>a'
endfunction

function s:RutedKeyMapping(key, shortcut)
  execute 'inoremap <silent> '. a:shortcut .  ' <esc>:call <SID>RutedSwitchMode("' . a:key . '")<CR>a'
endfunction

""
" Public functions
""

" Start ruted.
function StartRuted()
  call s:RutedInitialize()
  " get list of modes
  silent let l:json = system(s:ruted_command . ' list --json')
  let s:ruted_available_modes = json_decode(l:json)

  " Create a shortcut and menu item for each mode and also one for NONE mode.
  " First add NONE mode
  let l:name = "no mode"
  let l:shortcut = s:ruted_mode_selection_key . '<Space>'

  call s:RutedKeyMapping(s:RUTED_NONE_MODE, l:shortcut)
  call s:RutedMenuItem(s:RUTED_NONE_MODE, l:name, l:shortcut)

  " Add menu separator so NONE mode stands out.
  execute 'inoremenu ' . s:RUTED_MENU . '.-sep- :'

  " Add all other modes.
  for l:key in keys(s:ruted_available_modes)
    let l:name = s:ruted_available_modes[l:key]
    let l:shortcut = s:ruted_mode_selection_key . l:key

    call s:RutedKeyMapping(l:key, l:shortcut)
    call s:RutedMenuItem(l:key, l:name, l:shortcut)
  endfor

  " Notify the plugin that it has been initialized and ready to use.
  let s:ruted_on = 1
endfunction

" Stop ruted.
function StopRuted()
  for l:key in keys(s:ruted_available_modes)
    execute 'iunmap' . s:ruted_mode_selection_key . l:key
  endfor
  execute 'iunmap <silent> ' . s:ruted_mode_selection_key . '<Space>'

  if !empty(s:active_mode)
    call s:RutedUninstall(s:active_mode)
  endif

  let s:active_key = s:RUTED_NONE_MODE
  let s:active_mode = {}
  let s:ruted_on = 0
endfunction

function! RutedStatus()
  " Show the status of ruted as a string for use in a status line.
  if s:ruted_on
    if empty(s:active_mode)
      return  s:RUTED_NAME
    else
      return  s:RUTED_NAME . '=' . s:active_mode['name']
    end
  else
    return ''
  end
endfunction

function RutedListModes()
  echo "Key\tName"
  for l:key in keys(s:ruted_available_modes)
    let l:modeline = l:key . "\t" . s:ruted_available_modes[l:key]
    echo l:modeline
  endfor
endfunction


" Automatically start ruted when loading the ruted plugin.
call StartRuted()
